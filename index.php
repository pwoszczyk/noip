<?php
	require('config.php');

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, sprintf('http://dynupdate.noip.com/nic/update?hostname=%s', HOST));
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(sprintf('Authorization: Basic %s', base64_encode(USER.':'.PASS))));
	curl_exec($ch);
	echo "\n";
	if (curl_errno($ch)) {
	    echo 'ERROR #' . curl_errno($ch) . ': ' . curl_error($ch)."\n";
	}
	curl_close($ch);
?>